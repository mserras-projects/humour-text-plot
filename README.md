# Humour Plot

A simple exercise to plot humouristic vs non-humouristic representations
in a T-SNE spatial representation.

In addition, I want to test the gitlab pages feature. Just for fun :D

## Dataset

The humour vs non-humour dataset can be downloaded at [Kaggle](https://www.kaggle.com/datasets/deepcontractor/200k-short-texts-for-humor-detection), all credits to their 
original authors :)

## Pipeline

The pipeline in main.py follows the KISS principle:

    def run_pipeline():
        print('Loading the data...')
        data, labels = load_data(num_samples=20000)
        print('Extracting BERT representations')
        vectorial_representation = vectorize_texts(data)
        print('Extracting the T-NSE representation')
        tsne_representation = TSNE(n_components=2,
                                   learning_rate='auto', init='random').fit_transform(vectorial_representation)
        print('Plotting everything')
        save_figure(tsne_representation, labels, data)

The vectorial representation is done with the DistilBert uncased english model.

# License - MIT

Copyright 2022, M. Serras

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.