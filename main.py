
from sklearn.manifold import TSNE

from src.data import load_data
from src.plot import save_figure
from src.vectorize import vectorize_texts


def run_pipeline():
    print('Loading the data...')
    data, labels = load_data(num_samples=20000)
    print('Extracting BERT representations')
    vectorial_representation = vectorize_texts(data)
    print('Extracting the T-NSE representation')
    tsne_representation = TSNE(n_components=2,
                               learning_rate='auto', init='random').fit_transform(vectorial_representation)
    print('Plotting everything')
    save_figure(tsne_representation, labels, data)


if __name__ == '__main__':
    run_pipeline()
