import os

current_dir = os.path.dirname(os.path.abspath(__file__))
data_dir = os.path.join(current_dir, '..', 'data')
public_dir = os.path.join(current_dir, '..', 'public')
