import csv
import os
import typing

from src import data_dir

csv_file = os.path.join(data_dir, "dataset.csv")


def load_data(num_samples: typing.Optional[int] = None):
    data = []
    with open(csv_file) as f:
        reader = csv.reader(f)
        for row in reader:
            data.append(
                (row[0], 1 if row[1] == 'True' else 0)
            )
    # Discard the headers
    useful_data = data[1:]
    if num_samples:
        useful_data = useful_data[:num_samples]
    texts = [row[0] for row in useful_data]
    labels = [row[1] for row in useful_data]
    return texts, labels
