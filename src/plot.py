import os
import typing

import numpy as np
import pandas
import plotly.express as px

from src import public_dir


def save_figure(data: typing.List[np.ndarray], labels: typing.List[int], texts: typing.List[str]):
    data_frame = {"text": texts, 'x': [point[0] for point in data], 'y':[point[1] for point in data],
                  'color': ["Humour" if label == 1 else "No-Humour" for label in labels]}
    df = pandas.DataFrame(data=data_frame)
    print(df)
    fig = px.scatter(df, x='x',
                     y='y',
                     color='color', hover_data=['text'],
                     )

    fig.write_html(os.path.join(public_dir, "scatter_plot.html"))
