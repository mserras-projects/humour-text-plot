import typing

import numpy as np
from transformers import DistilBertTokenizer, DistilBertModel
import torch

tokenizer = DistilBertTokenizer.from_pretrained("distilbert-base-uncased")

model = DistilBertModel.from_pretrained("distilbert-base-uncased")


def vectorize_with_bert(input_text: str) -> np.ndarray:
    """
    Given a list of texts returns their pooler layer representation.
    For the distilled version of BERT this is equivalent of the hidden representation of the first
    item of the sequence, which is the [CLS] token representation.

    :param input_text: str
    :return: np.ndarray of shape N x Embedding_Dim where N is the amount of input texts
    """
    inputs = tokenizer([input_text], return_tensors="pt", truncation=True, padding=True)
    outputs = model(**inputs)

    last_hidden_states = outputs.last_hidden_state.detach().numpy()
    # Shape: Num_sentences x num_tokens x embedding

    return last_hidden_states[0, 0]


def vectorize_texts(texts: typing.List[str]) -> np.ndarray:
    """
    Vectorise the received text arrays
    """
    return np.vstack([vectorize_with_bert(text) for text in texts])
